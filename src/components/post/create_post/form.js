import React from 'react';
import {Form, Button, Alert} from 'react-bootstrap';
import useForm from "./useForm"
import validate from "./validateForm";

const Create = () => {
    const {
        categories,
        newPost,
        errors,
        submitSucces,
        setSubmitSucces,
        handleSubmit,
        handleChange
    }
    = useForm(submit, validate);

    function submit() {}

    return (
        <Form onSubmit={handleSubmit}>
            <Alert show={submitSucces} variant="success" onClose={() => setSubmitSucces(false)} dismissible>
                <p className="mb-0">Post is succesvol verstuurd.</p>
            </Alert>
            <Form.Group controlId="postTitle.ControlInput1">
                <Form.Label className="labelPostForm">Berichtnaam</Form.Label>
                <Form.Control
                    className="postForm"
                    type="text"
                    name="title"
                    onChange={handleChange}
                    value={newPost.title}
                    placeholder="geen titel"
                    isInvalid={!!errors.title}
                />
                <Form.Control.Feedback type="invalid">
                    {errors.title}
                </Form.Control.Feedback>
            </Form.Group>
            <Form.Group controlId="postCategory.ControlSelect1">
                <Form.Label className="labelPostForm">Categorie</Form.Label>
                <Form.Control
                    className="postForm"
                    as="select"
                    name="category_id"
                    onChange={handleChange}
                    value={newPost.category_id}
                    isInvalid={!!errors.category_id}>
                    {categories.map(category =>
                        <option key={category.id} value={category.id}>{category.name}</option>
                    )}
                </Form.Control>
                <Form.Control.Feedback type="invalid">
                    {errors.category_id}
                </Form.Control.Feedback>
            </Form.Group>
            <Form.Group controlId="postContent.ControlTextarea1">
                <Form.Label className="labelPostForm">Bericht</Form.Label>
                <Form.Control
                    className="postForm"
                    as="textarea"
                    name="content"
                    value={newPost.content}
                    onChange={handleChange}
                    place rows={12}
                    feed
                    isInvalid={!!errors.content}
                />
                <Form.Control.Feedback type="invalid">
                    {errors.content}
                </Form.Control.Feedback>
            </Form.Group>
            <div className="d-flex justify-content-center">
                <Button className="prim-btn" type="submit">Bericht aanmaken</Button>
            </div>
        </Form>
    )
}

export default Create
