import {useEffect, useState} from "react";
import axios from "axios";
import {API, token} from "../../../constants";


const useForm = (callback, validate) => {
    const [categories, setCategoriesdata] = useState([]);
    const [newPost, setnewPostdata] = useState({title : "", content : "", category_id: ""});
    const [errors, setErrors] = useState({title : "", content : "", category_id: ""});
    const [submitSucces, setSubmitSucces] = useState(false);

    //Function is called when form is submitted and has no errors
    const handleSubmit = (event) => {
        event.preventDefault();
        //checks is valid
        setErrors(validate(newPost))

        callback()

        axios.post(API + "posts",
            {title:newPost.title, content:newPost.content, category_id:newPost.category_id},
            {headers:{token}})
            .then(res =>{
                console.log(res)
                setSubmitSucces(true);
                setnewPostdata({...newPost, title : "", content : "", category_id: ""});
            })
            .catch(error => {
                console.log(error)
            })
    }
    //Get values of the form when the value changes
    const handleChange = (event) =>{
        const {name,value} = event.target;
        setnewPostdata({
            ...newPost,
            [name]: value
        });
    }

    //Gets categories form api
    const fetchData =() => {
        const getCategory = axios.get(API+"categories",{headers:{token}})
        axios.all([getCategory])
            .then(axios.spread((...allData) =>{
                const allDataCategory = allData[0].data
                setCategoriesdata(allDataCategory)
            }))
            .catch(error =>{
                console.log(error)
            })
    }

    useEffect(() =>{
        fetchData()
    }, [])

    //Returns the following functions to view,
    return{
        categories,
        newPost,
        errors,
        submitSucces,
        setSubmitSucces,
        handleSubmit,
        handleChange,
    };
};
export default useForm;
