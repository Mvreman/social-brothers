export default function validate(newPost){
    let errors = {};

    if (!newPost.title) {
        errors.title = "Titel kan niet leeg zijn";
    }
    if (!newPost.category_id) {
        errors.category_id = "Catogorie kan niet leeg zijn";
    }
    if (!newPost.content) {
        errors.content = "Bericht kan niet leeg zijn";
    }
    return errors;
}
