import {useEffect,useState} from "react";
import axios from "axios";
import {API, token} from "../../../constants";

const usePost = () => {
    const [posts, setPostdata] = useState([]);
    const [nextPost, setNextPost] = useState(1);

    // gets posts by pageNr
    const fetchData = (pageNr) => {
        return axios
            .get(API+`posts?page=${pageNr}`,{headers:{token}})
            .then(({ data }) => {
                return data
            })
            .catch(error => {
                console.log(error)
            })}

    // fetches new posts with old posts and returns back
    const fetchNextPost = () => {
        fetchData(nextPost).then((postData) => {
            if (postData === undefined) return;
            const newPostData =[
                ...posts,
                ...postData
            ]
            setPostdata(newPostData || "Post data not found")
            setNextPost((prevState) => prevState +1)
        })
    }

    // calls fetchPost function each time the btn "Meer laden" is clicked
    useEffect(() =>{
        fetchNextPost()
        // eslint-disable-next-line
    }, [])

    //Returns posts and fetchNextPost to view
    return{
        posts,
        fetchNextPost,
    };
};

export default usePost;
