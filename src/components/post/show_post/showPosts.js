import React, {lazy, Suspense} from 'react';
import {Button, Row,} from "react-bootstrap";
import usePost from "./usePosts";

const CardPost = lazy(() => import("./card"));

const BaseCard = () => {
    const {posts, fetchNextPost} = usePost(submit);

    function submit() {}

    return (
        <div>
            <Suspense fallback={<h3 className="d-flex justify-content-center">Loading......</h3>}>
                <Row className="card-row">
                    {/*Maps each post in a Card */}
                    {posts.map((post) => (<CardPost key={post.id} post={post}/>))}
                </Row>
                <div className="d-flex justify-content-center">
                    <Button className="prim-btn" type="submit" onClick={fetchNextPost}>Meer laden</Button>
                </div>
            </Suspense>
        </div>
    )
};

export default BaseCard
