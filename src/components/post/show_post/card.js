import { Card, Col } from 'react-bootstrap';
import React from 'react';
import Moment from 'moment';


const CardPost = (props) => {

    return (
        <Col sm={5} className="card-col">
        <Card className="card-post">
            <div>
                <Card.Img src={props.post.img_url} />
                <Card.ImgOverlay>
                    <Card.Text>{Moment(props.post.created_at).format('DD-MM-YYYY')}</Card.Text>
                    {props.post.category && props.post.category.name &&
                        <Card.Text> {props.post.category.name}</Card.Text>
                    }
                </Card.ImgOverlay>
            </div>
            <Card.Body>
                <Card.Title>{props.post.title}</Card.Title>
                <Card.Text>{props.post.content}</Card.Text>
            </Card.Body>
        </Card>
        </Col>
    )
};

export default CardPost
