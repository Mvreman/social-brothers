import React from 'react';
import logo from './assets/logo-3.png';
import { CreateForm, ShowPost } from "./components/post/index";
import './static/index.css';
import { Row, Col, Card} from 'react-bootstrap';

function App() {
    return (
        <div className="App">

            <header className="header">
                {/*<img src={logo} className="App-logo" alt="logo" />*/}
                <img src={logo} className={"logo"} alt="logo"/>
            </header>
            <div className="container">
                <Row>
                    <Col>
                        <Card className="comp-card">
                            <CreateForm/>
                        </Card>
                    </Col>
                    <Col>
                        <Card className="comp-card">
                            <ShowPost/>
                        </Card>
                    </Col>
                </Row>
            </div>
        </div>
    );
}

export default App;
